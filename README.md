**Updates for 0.1.9** Addition of NPC stats (I've limited the automation of field calculation as much as possible, to allow flexibility). I've also reformatted the chat output, and am accepting feedback on the layout of the NPC sheet.

**Updates for 0.1.4**: Compendiums are now available for Zweihander. They are not complete yet, as the required data entry is extensive, but they're getting there, and will hopefully provide a guide for if you have to input your own items. Please remember that system-level compendiums will be overwritten on an update, so copy the compendium before you add to it. I've also added damage rolls to the weapon card, the ability to track Focuses, ancestries that automatically update the appropriate character bonuses (alas, it may be some time, if at all before Ancestry Traits are automated), injury calculation (just click the appropriate damage level on the damaged characters track and the system will roll chaos dice and then roll on the appropriate Injury table - see below), and - finally - a function to automatically roll inital attributes (Shift-Click the link in the Main tab that reads "Generate Attributes").

To make the injury tables work (and, yes, I'm working on a more elegant solution): import the full contents of the Tables compendium. Once imported, open each of the three injury tables and click the "Update Table" button in the bottom left. Then, the Injury rolls should work (please note that the name of the injury in the chat message can be dragged to the character sheet to apply the Injury).

This is a work in progress implementation of Zweihander/Flames of Freedom on Foundry VTT. 

Things that Currently Work:
    1. Rolls for Skills by clicking Skill name
    2. Creation of Professions to provide ability to increase Skills/Bonuses/Talents (for testing, this is commented out except for one skill and one Bonus)
    3. Creation of weapons and armor (armor automatically calculates into Damage Thresholds)
    4. Automatic calculation of bonuses, thresholds, initiative, and movement


Things I know need to be done:
    1. Formatting of Chaos/Order Ranks as dots, instead of digits
    2. Addition of damage roll button to attack output card
    3. General formatting of sheet (to make it look less garbage)
    4. Creation of specialized, compact, non-automating NPC sheet
    5. Addition of a way to remove weapon qualities from the character sheet, so the reminder text disappears

The general flow of advancement:
    1. Assign Attributes
    2. Create/Drag Profession onto character (this will allow for advances to be done through the Profession sheet)
    3. Select advances to purchase by clicking the + button next to the advance you want (until we're official, I would expect that Talents will have to be manually dragged onto the character sheet)
    4. If all goes well, the number of RP spent on your current career should update, along with your remaining RP on the main character sheet.
    

