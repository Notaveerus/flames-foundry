export async function rollChaos(chaosdice){
    var i;
    let chaossuccess = 0;
    if (chaosdice){
        for (i=1; i <= chaosdice; i++){
            let r = new Roll('d6');
            r.roll();
            if (game.dice3d){
                await game.dice3d.showForRoll(r);}
            console.log(r.total)
            if (r.total == 6){
                chaossuccess += 1;
            } 
        }
        return chaossuccess;
    }

}