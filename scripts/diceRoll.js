import {rollOptions} from './rollOptions.js';
import {flipToFail} from './flipToFail.js';
import {chatMessage} from './chat-output.js';
import {flipToSucceed} from './flipToSucceed.js';
import {assistRoll} from './assistRoll.js';

export async function skillRoll(inputs){
    console.log(inputs);
    const actor = inputs.actor;
    inputs.actorid = actor.id;
    console.log(actor.data.data.attributes)
    let basechance = actor.data.data.attributes[inputs.attribute].value;
    console.log(basechance);
    inputs.flipped = false;
    let ranks = actor.data.data.skills[inputs.attribute][inputs.skill]
    let skillchance = 10*ranks;
    if (inputs.type == "advanced" && skillchance < 1){
     inputs.flip = "fail";}
    let modifiers = await rollOptions(inputs);
    let difficulty = modifiers.difficulty + modifiers.miscellaneous;


    if (modifiers.focus == false){
    if (actor.data.data.peril.value > 1 && ranks >=1){
        ranks -= 1;
    }
    if (actor.data.data.peril.value >2 && ranks >= 1){
        ranks -= 1;
        }
    if (actor.data.data.peril.value >3 && ranks >= 1){
        ranks -= 1;
    }
}
    skillchance = 10*ranks;
    inputs.skillchance = skillchance
    let chance = Number(basechance) + Number(skillchance) + Number(difficulty);
    console.log(`Success Chance is: ${chance}`);
    let r = new Roll('d100');
    inputs.chance = chance;


    r.roll()
    if (game.dice3d){
        await game.dice3d.showForRoll(r);}
    let total = r.total;
    inputs.originalroll = r.total
    console.log(modifiers.flipped)
    if(modifiers.assist == true){
        total = await assistRoll(total);
    }
    if (modifiers.flipped =="fliptofail")
    {total = flipToFail(total);}
    else if (modifiers.flipped == "fliptosucceed"){
        total = flipToSucceed(total);
    }
    inputs.flipped = modifiers.flipped;
    inputs.opposed = modifiers.opposed;
    inputs.total=total;
    let attributename = inputs.attribute + "bonus";
    inputs.successlevels = Math.floor(total/10) + actor.data.data[attributename];
    inputs.modifiers = modifiers;
    chatMessage(inputs);




};
