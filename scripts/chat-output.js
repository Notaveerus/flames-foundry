export function chatMessage(inputs){
    console.log(inputs)
    let outerHTML = `
                  <div class="rolltemplate-zweihander">
                    <div class="template-wrap">
                      <div class="template-grid">
                        {}
                      </div>
                    </div>
                  </div>`

    var critical;
    var outcome;
    var success = false;
    let skill = CONFIG.flamesSkills.skillnames[inputs.skill].displayname;
    if (inputs.total%11 == 0 || inputs.total == 1 || inputs.total == 100){
        critical = true;
    }
    else{
        critical = false;
    }
    if (inputs.total <= inputs.chance){
      success = true;
    }


    let innerHTML = "";
        innerHTML = `<div class="template-grid-item template-span-all-four template-center-contents template-dark-red">
                        <span class="template-header">${inputs.actor.name}</span>
                </div>
                <div class="template-grid-item template-span-one-two">
                        <span class="">${inputs.attribute}: </span>${inputs.actor.data.data.attributes[inputs.attribute].value}
                </div>
                <div class="template-grid-item template-span-three-four">
                        <span>${inputs.skill}: </span>${inputs.skillchance}
                </div>
                <div class="template-grid-item template-span-one-two">
                        <span>Difficulty: </span>${inputs.modifiers.difficulty}
                </div>
                <div class="template-grid-item template-span-one-two">
                        <span data-i18n="chaos:">Chaos:</span>
                </div>
                <div class="template-grid-item template-span-one-two template-center-contents">
                        <span class="">${inputs.total}</span><br>
                         ${critical && success ? '<span class="template-smaller template-green template-bold" data-i18n="CRITICAL-SUCCESS-u">CRITICAL SUCCESS</span>':''}
                         ${critical && !success ? '<span class="template-smaller template-red template-bold" data-i18n="CRITICAL-FAILURE-u">CRITICAL FAILURE</span>': ''}
                    </div>
                <div class="template-grid-item template-span-three-four template-center-contents">
                        <span>${inputs.chance}</span><br>
                        <span class="template-smaller" data-i18n="rating">Rating</span>
                </div>
                `
      if(inputs.details){
        innerHTML+= `<div class="template-grid-item template-span-all-four">
                        <span class="template-smaller">${inputs.details}</span>
                    </div>`
      }
      if(inputs.critsucc){
        innerHTML+= `<div class="template-grid-item template-span-all-four">
                        <span class="template-smaller">Critical Success: </span>${inputs.critsucc}
                    </div>`
      }
      if(inputs.critfail){
        innerHTML+= `<div class="template-grid-item template-span-all-four">
                        <span class="template-smaller">Critical Failure: </span>${inputs.critfail}
                    </div>`
      }





    // html += `<div class="output roll-output critical-${critical}-${outcome}"`;
    // html += `<span title="Unmodified Roll: ${inputs.originalroll}">${inputs.total}</span></div><div class="output chance-output">Success Chance was ${inputs.chance}</div>`
    // if (critical == true && inputs.total <= inputs.chance){
    //     html += `<div class="output critical-output critical-success">You Scored a Critical Success!</div>`;
    // }
    // else if (critical == true){
    //     html+=`<div class="output critical-output critical-failure">You Got a Critical Failure</div>`;
    // }
    // if (inputs.total <= inputs.chance && inputs.opposed == true){
    // html += `<div class="output success-levels">You scored ${inputs.successlevels} Success Levels</div>`;}
    // if (inputs.flipped == "fliptofail"){
    //     html +=`<div class="output flipped-output">The Result was Flipped to Fail</div>`;
    // }
    // else if (inputs.flipped == "fliptosucceed")
    //     {html +=`<div class="output flipped-output">The Result was Flipped to Succeed</div>`}
    // if (inputs.attack){
    //     html +=`<div class="output"><button class="damage-roll" data-actorid="${inputs.actorid}" data-weaponid="${inputs.weaponid}">Roll Damage</button></div>`;}
    // if (!inputs.rerolled && !inputs.attack){
    //     html +=`<div><button class="skill-roll" data-actorid="${inputs.actorid}" data-skill="${inputs.skill}" data-type="${inputs.type}" data-att="${inputs.attribute}">Click to Re-roll With Fortune</button></div>`
    // }
    // else if(!inputs.rerolled && inputs.attack){
    //     html +=`<div><button class="attack-roll" data-weaponid="${inputs.weaponid}" data-actorid="${inputs.actorid}" data-skill="${inputs.skill}" data-type="${inputs.type}" data-att="${inputs.attribute}">Click to Re-roll With Fortune</button></div>`
    // }
    var html = outerHTML.format(innerHTML)
    ChatMessage.create({ content: html, speaker: ChatMessage.getSpeaker({alias: inputs.actor.name}) });
    AudioHelper.play({src: "sounds/break-metal.mp3", volume: 0.8, autoplay: true, loop:false}, true);
}
