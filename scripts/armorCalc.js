export function armorCalc(actor){
    console.log(`The actor is ${actor}`)
    const armor = actor.items.filter(function(item) {return (item.type == "Armor" && item.equipped == false)} );
    let runningtotal = 0;
    
    for (const element of armor) {
    let countArmor = Number(element.data.data.armorvalue);
    let total = countArmor;
    runningtotal += total;
    console.log(`Armor total is ${total}`);}

    return runningtotal;

};