import flamesTracker from "../modules/tracker.js";
import {damageRoll} from "./damageRoll.js";
import {skillRoll} from "./diceRoll.js";
import { rerollDamage } from "./rerollDamage.js";
import { rollOptions } from "./rollOptions.js";

export function chatListeners(html){
    html.on("click", ".damage-roll", ev => {
        
    let li = $(ev.currentTarget).parents(".item-name");
      const itemId = $(ev.currentTarget).attr("data-weaponid");
      console.log(itemId);
      const actor = $(ev.currentTarget).attr("data-actorid");
      let actorData = game.actors.get(actor);
      const item = actorData.getOwnedItem(itemId);
      let inputs = {"damagebonus": item.data.data.damagebonus, "damage": item.data.data.damage, "actor": actorData};
      damageRoll(inputs);

     })

     html.on("click", ".skill-roll", ev => {
        const att = $(ev.currentTarget).attr("data-att");
        const skill = $(ev.currentTarget).attr("data-skill");
        const type = $(ev.currentTarget).attr("data-type");
        const actor = $(ev.currentTarget).attr("data-actorid");
        let actorData = game.actors.get(actor);
        let inputs = {"attribute": att, "skill": skill, "actor": actorData, "type": type, "rerolled": true};

        if (!game.user.hasRole(game.settings.get('flames', 'misfortunePermissionLevel'))) {
            ui.notifications.error(game.i18n.localize('flames.notifications.fortuneinvalidpermissions'));
            return false;
          }
          let misfortune = game.settings.get('flames', 'misfortune');
          let fortune = parseInt(document.getElementById('flames-track-fortune').value);
          if (fortune === 0) {
            ui.notifications.warn('There\'s No Fortune Left!');
            return false;
          }
          fortune = fortune - 1;
          misfortune = misfortune +1;
          game.settings.set('flames', 'fortune', fortune);
          game.settings.set('flames', 'misfortune', misfortune);
          document.getElementById('flames-track-misfortune').value = misfortune;
        document.getElementById('flames-track-fortune').value = fortune;
          

        skillRoll(inputs);
    });

    html.on("click", ".damage-reroll", ev => {
      let lowDamage = Number($(ev.currentTarget).attr("data-lowDamage"));
       let damage = Number($(ev.currentTarget).attr("data-damage"));
      // const att = $(ev.currentTarget).attr("data-att");
      // const skill = $(ev.currentTarget).attr("data-skill");
      // const type = $(ev.currentTarget).attr("data-type");
      const actor = $(ev.currentTarget).attr("data-actorid");
      let actorData = game.actors.get(actor);

      let inputs = {"lowDamage": lowDamage, "damage": damage, "actorData": actorData};

      rerollDamage(inputs);
      
  
  });

  html.on("click", ".attack-roll", ev => {
    const att = $(ev.currentTarget).attr("data-att");
        const skill = $(ev.currentTarget).attr("data-skill");
        const type = $(ev.currentTarget).attr("data-type");
        const actor = $(ev.currentTarget).attr("data-actorid");
        const weaponid = $(ev.currentTarget).attr("data-weapontype");
        let actorData = game.actors.get(actor);
    
    let inputs = {"attribute": att, "skill": skill, "actor": actorData, "type": type, "attack": true, "weaponid": weaponid, "rerolled": true };
    console.log(inputs);
    skillRoll(inputs);
});

    
};