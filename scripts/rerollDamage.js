export async function rerollDamage(inputs){
    if (!game.user.hasRole(game.settings.get('flames', 'misfortunePermissionLevel'))) {
        ui.notifications.error(game.i18n.localize('flames.notifications.fortuneinvalidpermissions'));
        return false;
      }
      let misfortune = game.settings.get('flames', 'misfortune');
      let fortune = parseInt(document.getElementById('flames-track-fortune').value);
      if (fortune === 0) {
        ui.notifications.warn('There\'s No Fortune Left!');
        return false;
      }
      fortune = fortune - 1;
      misfortune = misfortune +1;
      game.settings.set('flames', 'fortune', fortune);
      game.settings.set('flames', 'misfortune', misfortune);
      document.getElementById('flames-track-misfortune').value = misfortune;
    document.getElementById('flames-track-fortune').value = fortune;
    
      let lowDamage = inputs.lowDamage;
      let actorData = inputs.actorData;
      let damage = inputs.damage;


    let adjustment = Number(6 - lowDamage);
    damage += adjustment;
    let r = new Roll(`d6x`);
    r.roll();
    if (game.dice3d){
      await game.dice3d.showForRoll(r);}
    console.log(r.total);
    let newDamage = Number(damage + r.total)
  const html = `<div>You did <span>${newDamage}</span> damage!</div>`
  ChatMessage.create({ content: html, speaker: ChatMessage.getSpeaker({alias: actorData.name})      });
      

}