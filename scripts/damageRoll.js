export async function damageRoll(inputs){
    let actor = inputs.actor;
    let bonus = inputs.damagebonus;
    const att = actor.data.data[bonus];


    let rollNumber = `1d6x+${inputs.damage}`;

    let r = await new Roll(rollNumber).roll();
    let results = r.dice[0].results.map(r => r.result)
    let min = Math.min(...results)


    if (game.dice3d){
        await game.dice3d.showForRoll(r);}

    let totalDamage = att + r.total;
    const html = `<div>You did <span title="${r.result}">[[1d6x+${inputs.damage + att}]]</span> damage!</div> `
    ChatMessage.create({ content: html, speaker: ChatMessage.getSpeaker({alias: actor.name})      });

}
