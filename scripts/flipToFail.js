export function flipToFail(input){
    let tens = Math.floor(input/10);
    let ones = input % 10;
    let flippedResult = Number((ones * 10)) + Number(tens)
    if (input < flippedResult){
    return flippedResult;}
    else{return input}
}