export async function assistRoll(input){
    let r = new Roll('d10-1');
    r.roll();
    if (game.dice3d){
        await game.dice3d.showForRoll(r);}
    let newtens = r.total;
    console.log(`New Tens is ${newtens}`);
    let tens = Math.floor(input/10);
    let ones = input % 10;
    let assistedResult = Number((newtens*10) + ones);
    console.log(`Assisted Result was ${assistedResult}`)
    if (assistedResult < input){
        return assistedResult;
    }
    else{
        return input;
    }
}