export async function rollOptions(inputs){
    var flip;
    if (inputs.flip == "fail"){
        flip = `<div><input type="radio" id="fliptofail" name="flip" value="fliptofail" checked>Flip to Fail</input>
        <input type="radio" id="noflip" name="flip" value="noflip" >No Flip</input>
        <input type="radio" id="fliptosucceed" name="flip" value="fliptosucceed" >Flip to Succeed</input></div>`}
    else if(inputs.flip == "succeed"){
        flip = `<div><input type="radio" id="fliptofail" name="flip" value="fliptofail">Flip to Fail</input>
        <input type="radio" id="noflip" name="flip" value="noflip" >No Flip</input>
        <input type="radio" id="fliptosucceed" name="flip" value="fliptosucceed" checked>Flip to Succeed</input></div>`

    }
    else{
        flip = `<div><input type="radio" id="fliptofail" name="flip" value="fliptofail">Flip to Fail</input>
        <input type="radio" id="noflip" name="flip" value="noflip" checked>No Flip</input>
        <input type="radio" id="fliptosucceed" name="flip" value="fliptosucceed" >Flip to Succeed</input></div>`
    }
    var html = `<div>Roll Modifier: <select id="difficulty_box" >
                  <option value="30">Trivial +30%</option>
                  <option value="20">Easy +20%</option>
                  <option value="10">Routine +10%</option>
                  <option value="0" selected>Normal +0%</option>
                  <option value="-10">Challenging -10%</option>
                  <option value="-20">Hard -20%</option>
                  <option value="-30">Arduous -30%</option>
                  </select></div>
                  <div>Miscellaneous Modifiers?<input type="number" class="long-input" id="misc_mod" /></div>
                  <div>Focus Applies?<input type="checkbox" id="focus_box"></div> 
                  <div>Roll Assisted?<input type="checkbox" id="assist_box"></div> 
                  <div>Opposed Roll?<input type="checkbox" id="opposed_box"></div> 
                  `;
    html += flip
    let miscmod = await new Promise(resolve => {
        new Dialog({
            title: "Difficulty Modifiers",
            content: html,
            buttons: {
                ok: {
                    label: "OK",
                    callback: () => {
                        resolve({
                            "difficulty": document.getElementById("difficulty_box").value,
                            "miscellaneous": document.getElementById("misc_mod").value,
                            "focus": document.getElementById("focus_box").checked,
                            "assist": document.getElementById("assist_box").checked,
                            "opposed": document.getElementById("opposed_box").checked,
                            "flipped": document.querySelector('input[type=radio][name=flip]:checked').value
                        })
                    }
                }
            },
            default:"ok"
        }).render(true);
    });
    return miscmod;
}