export async function getPeril(numdice){
    var html;
    if (numdice == 1){
    html = 'How many Dice? <select id="peril_box"><option value="1" selected type="number">1</option><option value="2">2</option><option value="3">3</option></select>'
    }
    else if (numdice == 2){
    html = '<select id="peril_box"><option value="1">1</option><option value="2" selected>2</option><option value="3">3</option></select>'
    }
    else {
    html = '<select id="peril_box"><option value="1">1</option><option value="2">2</option><option value="3" selected>3</option></select>'
    }
    console.log(html);
    let miscmod = await new Promise(resolve => {
        new Dialog({
            title: "Peril Selection",
            content: html,
            buttons: {
                ok: {
                    label: "OK",
                    callback: () => {
                        resolve({
                            "perillevel": Number(document.getElementById("peril_box").value),
                        })
                    }
                }
            },
            default:"ok"
        }).render(true);
    });
    console.log(miscmod)
    return miscmod;

}
