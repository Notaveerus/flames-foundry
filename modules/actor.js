export default class flamesActor extends Actor {


//   static async create(data, options = {}) {
//     if(this.data.type === 'PC'){
//     const dataArray = (data instanceof Array ? data : [data]).map((itemData) => {
//         const expanded = expandObject(itemData);
//         setProperty(expanded, "token.actorLink", true);
//         return expanded;
//     });
//     return super.create(dataArray, options);}
// }

    prepareData(){
        super.prepareData();
        const actorData = this.data;
        const data = actorData.data;


this._prepareCharacterData(actorData);


        }

        _prepareCharacterData(actorData) {
          const data = actorData.data;
          const professions = this.items.filter(function(item) {return (item.type == "Profession")} );
          data.armorrating = this.armorCalc();
          data.combatbonus = Math.floor(data.attributes.combat.value/10) + Number(data.attributes.combat.bonus) + Number(this.bonusCalc("combat"));
          data.brawnbonus = Math.floor(data.attributes.brawn.value/10) + Number(data.attributes.brawn.bonus) + Number(this.bonusCalc("brawn"));
          data.agilitybonus = Math.floor(data.attributes.agility.value/10) + Number(data.attributes.agility.bonus) + Number(this.bonusCalc("agility"));
          data.perceptionbonus = Math.floor(data.attributes.perception.value/10) + Number(data.attributes.perception.bonus) + Number(this.bonusCalc("perception"));
          data.intelligencebonus = Math.floor(data.attributes.intelligence.value/10) + Number(data.attributes.intelligence.bonus) + Number(this.bonusCalc("intelligence"));
          data.willpowerbonus = Math.floor(data.attributes.willpower.value/10) + Number(data.attributes.willpower.bonus) + Number(this.bonusCalc("willpower"));
          data.fellowshipbonus = Math.floor(data.attributes.fellowship.value/10) + Number(data.attributes.fellowship.bonus) + Number(this.bonusCalc("fellowship"));
          data.encumbrancelimit = data.brawnbonus +3;
          data.encumbrance = this.encumbranceCalc();
          data.overage = data.encumbrance - data.encumbrancelimit;
          data.overageModifier = this.overageCalc();
          data.initiative = data.perceptionbonus + 3 - data.overageModifier;
          data.movement = data.agilitybonus + 3 - data.overageModifier;
          data.tier = professions.length;
          data.perilthreshold = data.willpowerbonus + 3;
          data.perilthreshold2 = data.perilthreshold + 6;
          data.perilthreshold3 = data.perilthreshold2 + 6;
          data.perilthreshold4 = data.perilthreshold3 + 6;
          data.woundthreshold = data.brawnbonus + data.armorrating;
          data.woundthreshold2 = data.woundthreshold + 6;
          data.woundthreshold3 = data.woundthreshold2 + 6;
          data.woundthreshold4 = data.woundthreshold3 + 6;
          data.rpremaining = this.RPCalc();
          data.spells = (this.items.filter(function(item) {return (item.type == "Spell")} )).length;
          data.numberfocuses = (this.items.filter(function(item) {return (item.type == "Focus")} )).length;



        }

        armorCalc(){
          const armor = this.items.filter(function(item) {return (item.type == "Armor")} );
          let runningtotal = 0;
          for (const element of armor) {
          let countArmor = Number(element.data.data.armorvalue);
          let total = countArmor;
          runningtotal += total;}
          return runningtotal;

      };

      bonusCalc(attribute){
        const ancestry = this.items.filter(function(item) {return (item.type == "Ancestry")} );
        if (ancestry.length >0){
        let bonus = 0;
        if (ancestry[0].data.data[attribute]){
          bonus = ancestry[0].data.data[attribute]
        }
        return bonus;}
        else{return 0}
      }

      RPCalc(){
        let rpearned = this.data.data.rpearned;
        let rpmisc = this.data.data.rpmisc;
        const professions = this.items.filter(function(item) {return (item.type == "Profession")} );
        let runningtotal = 0;
        for (const element of professions) {
        let countRP = Number(element.data.data.rpspent);
        let total = countRP;
        runningtotal += total;}
        return (rpearned - runningtotal - rpmisc);

    };

    encumbranceCalc(){
      const stuff = this.items.filter(function(item) {return (item.type == "Armor" || item.type == "Weapon" || item.type == "Trapping")} );
      let runningtotal = 0;
      for (const element of stuff) {
      let countStuff = Number(element.data.data.encumbrance);
      let total = countStuff;
      runningtotal += total;}
      return runningtotal;
    }

    overageCalc(){
      var overageModifier;
      if (this.data.data.overage > 0){
          overageModifier = this.data.data.overage;
      }
      else {overageModifier = 0}

      return overageModifier;

    }
    }

    String.prototype.format = function () {
  var i = 0, args = arguments;
  return this.replace(/{}/g, function () {
    return typeof args[i] != 'undefined' ? args[i++] : '';
  });
};
