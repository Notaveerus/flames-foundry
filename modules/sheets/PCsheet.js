import {skillRoll} from '/systems/flames/scripts/diceRoll.js';
import {damageRoll} from '/systems/flames/scripts/damageRoll.js';
import {rollChaos} from '/systems/flames/scripts/rollChaos.js';
import { rollOptions } from '../../scripts/rollOptions.js';
import {generateAttributes} from '../../scripts/generateAttributes.js';
import {flamesSkills} from '../config.js';
import {chaosChange} from '../../scripts/chaosChange.js';
import {orderChange} from '../../scripts/orderChange.js';
import {getPeril} from '../../scripts/getPeril.js';
import {rollPeril} from '../../scripts/rollPeril.js'

export default class PCSheet extends ActorSheet {
    get template() {
        return `systems/flames/templates/sheets/pc-sheet.hbs`;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
          scrollY: [
            ".Stats .inventory-list",
            ".Professions .inventory-list",
            ".Combat .inventory-list",
            ".Magick .inventory-list"
          ],
          tabs: [{navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description"}]
        });
      }


    getData() {
        const data = super.getData();
        data.professions = data.items.filter(function(item) {return item.type == "Profession"} );
        data.weapons = data.items.filter(function(item) {return item.type == "Weapon"} );
        data.armor = data.items.filter(function(item) {return item.type == "Armor"} );
        data.trappings = data.items.filter(function(item) {return item.type == "Trapping"} );
        data.conditions = data.items.filter(function(item) {return item.type == "Condition"} );
        data.talents = data.items.filter(function(item) {return item.type == "Talent"} );
        data.ancestries = data.items.filter(function(item) {return item.type == "Ancestry"} );
        data.spells = data.items.filter(function(item) {return item.type == "Spell"} );
        data.focuses = data.items.filter(function(item) {return item.type == "Focus"} );
        data.descriptions = data.items.filter(function(item) {return item.type == "Profession" || item.type == "Talent" || item.type == "Condition" || item.type == "Disease"})
        data.gameType = game.settings.get('flames', 'gameName');
        data.skillNames = CONFIG.flamesSkills.skillnames;
        let groupName = "data.damage.value";
        let damagechoices = {0: "Unharmed", 1: "Lightly Wounded", 2: "<a class='roll-chaos-wounds' data-numdice='1' data-type='Moderate'> Moderately Wounded</a>", 3: "<a class='roll-chaos-wounds' data-numdice='2' data-type='Serious'>Seriously Wounded</a>", 4: "<a class='roll-chaos-wounds' data-numdice='3' data-type='Grievous'>Grievously Wounded</a>", 5: "Slain!"};
        data.damagechoices = damagechoices;
        let perilgroupName = "data.peril.value";
        let perilchoices = {0: "Unharmed", 1: "Imperiled", 2: "Ignore 1 Skill Rank", 3: "Ignore 2 Skill Ranks", 4: "Ignore 3 Skill Ranks", 5: "Incapacitated!"};
        data.perilchoices = perilchoices;
        console.log(data);
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html)

            html.find('.skill-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const type = $(ev.currentTarget).attr("data-type");
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type};
            skillRoll(inputs);
        });

        html.find('.attack-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            let type = "advanced";
            if (skill == "simplemelee" || skill == "simpleranged"){
              type = "basic";
            }
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": type, "attack": true, "weaponid": item.id };
            console.log(inputs);
            skillRoll(inputs);
        });
        html.find('.spell-roll').click(ev => {
            const att = $(ev.currentTarget).attr("data-att");
            const skill = $(ev.currentTarget).attr("data-skill");
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            const details = $(ev.currentTarget).attr("data-details");
            const critsucc = $(ev.currentTarget).attr("data-critsucc");
            const critfail = $(ev.currentTarget).attr("data-critfail");
            let inputs = {"attribute": att, "skill": skill, "actor": this.actor, "type": "spell", "attack": true, "weaponid": item.id, "details":details,"critsucc":critsucc,"critfail":critfail};
            console.log(inputs);
            skillRoll(inputs);
        });
        html.find('.item-create').click(this._onItemCreate.bind(this));
        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item-name");
            const item = this.actor.getOwnedItem(li.data("itemId"));
            item.sheet.render(true);
        });

        html.find('.roll-chaos-wounds').click(ev => {
          let numdice = $(ev.currentTarget).attr("data-numdice");
          const tabletype = $(ev.currentTarget).attr("data-type");
          // diceobject = getPeril(numdice);
          // numdice = diceobject.perillevel;
          // console.log(numdice)
          let output = rollChaos(numdice);
          const tablename = tabletype + ` Injury Table`;
          if (output > 0){
          game.tables.getName(tablename).draw();}
          else{
            ChatMessage.create({ content: "You did not take an injury!", speaker: ChatMessage.getSpeaker({alias: this.actor.name})      });
          }
      });


      html.find('.generate-attributes').click(ev => {
        if (ev.shiftKey){
        generateAttributes(this.actor)}
        else (ui.notifications.info("Shift-Click to Regenerate Your Attributes"))
      });

      html.find('.roll-peril').click(ev => {
        rollPeril(this.actor);

      });



html.find('.item-delete').click(ev => {
    console.log(ev.currentTarget);
    let li = $(ev.currentTarget).parents(".item-name"),
    itemId = li.attr("data-item-id");
    console.log(li);
    console.log(`itemID =0 ${itemId}`);
    this.actor.deleteEmbeddedEntity("OwnedItem", itemId);
  });

  html.find('.profession-delete').click(ev => {
    console.log(ev.currentTarget);
    let li = $(ev.currentTarget).parents(".item-name");
    const itemId = li.attr("data-item-id");
    const item = this.actor.getOwnedItem(li.data("itemId"));
    let skillnumber = "skill0";
    var i;
    for(i = 1; i<11; i++){
      skillnumber = `skill${i}`;
      if (item.data.data[skillnumber].skillpurchased == true){
        ui.notifications.error("Skills Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<8; i++){
      skillnumber = `bonus${i}`;
      if (item.data.data[skillnumber].bonuspurchased == true){
        ui.notifications.error("Bonus Remaining to Remove");
        return;
      }
    }
    for(i = 1; i<4; i++){
      skillnumber = `talent${i}`;
      if (item.data.data[skillnumber].talentpurchased == true){
        ui.notifications.error("Talent Remaining to Remove");
        return;
      }
    }
    this.actor.deleteEmbeddedEntity("OwnedItem", itemId);

  });

    html.find('.damage-roll').click(ev => {
      let li = $(ev.currentTarget).parents(".item-name");
      const itemId = li.attr("data-item-id");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      let inputs = {"damagebonus": item.data.data.damagebonus, "damage": item.data.data.damage, "actor": this.actor};
      console.log(inputs)
      damageRoll(inputs);
  });

    // console.log(li);
    // console.log(`itemID =0 ${itemId}`);
    // this.actor.deleteEmbeddedEntity("OwnedItem", itemId);

    html.find('.chaos-increase').click(ev => {
      const newvalue = $(ev.currentTarget).attr("data-ranks");
      let inputs = {"newvalue": newvalue, "actor": this.actor}
      chaosChange(inputs);
  });

  html.find('.chaos-reduce').click(ev => {
    const newvalue = $(ev.currentTarget).attr("data-ranks");
    let inputs = {"newvalue": newvalue, "actor": this.actor}
    chaosChange(inputs);
});

html.find('.order-increase').click(ev => {
  const newvalue = $(ev.currentTarget).attr("data-ranks");
  let inputs = {"newvalue": newvalue, "actor": this.actor}
  orderChange(inputs);
});

html.find('.order-reduce').click(ev => {
const newvalue = $(ev.currentTarget).attr("data-ranks");
let inputs = {"newvalue": newvalue, "actor": this.actor}
orderChange(inputs);
});


}

_onItemCreate(event){
  event.preventDefault();
  const header = event.currentTarget;
    const type = header.dataset.type;
    const data = duplicate(header.dataset);
    data.moveType = data.movetype;
    data.spellLevel = data.level;
    const name = type == 'bond' ? game.i18n.localize("DW.BondDefault") : `New ${type.capitalize()}`;
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    delete itemData.data["type"];
    return this.actor.createOwnedItem(itemData);
}

}
